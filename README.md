<!-- -*- coding:utf-8-unix -*- -->
# Hello Parquet

Apache Parquetというカラムナフォーマットを活用したいと思い、そのRust実装であるparquetクレートを試してみました。

## 機能

- [x] CSVファイルからParquetファイルを作成
- [x] Parquetファイルで条件に一致する行を抽出（SQLの `SELECT .. WHERE ...` に相当）
- [ ] 抽出した行をカラムごとに集計（SQLの `SUM(カラム名)` などに相当）

**TODO**

- [ ]

## 動かしかた

### テストデータのダウンロード

本プロジェクトではテスト用の大きなデータとして以下のページで配布されているCSVファイルを使用します。

- TLC Trip Record Data
    - ニューヨーク市のタクシー乗車データ
    - 提供：New York City Taxi and Limousine Commission（TLC）
    - https://registry.opendata.aws/nyc-tlc-trip-records-pds/

どの月でもいいので、Yellow Taxi Trip Recordsをダウンロードします。

非圧縮のCSVデータとなっており、ファイルサイズは600MB程度、データ件数は700万件程度です。


### ビルドと実行

parquetクレートがRust nightlyツールチェインを必要とするため、Rustは`rustup`でインストールしておきます。
本プロジェクトには`rust-toolchain`ファイルが含まれていますので、`rustup`はそのファイルで指定されたnightlyツールチェインを自動的にダウンロードします。

```console
# x86_64系のシステムではSSE4.2系の命令が使用できる
$ export RUSTFLAGS='-C target-feature=+sse4.2'
$ cargo build --release --bin csv2par
$ cargo build --release --bin parselect1
$ cargo build --release --bin parmetadata

# CSVファイルからParquetファイルを作成する
$ ./target/release/csv2par yellow_tripdata_YYYY-MM.csv

# Parquetファイルで指定した日時の範囲にある行を抽出する
$ ./target/release/parselect1 yellow_tripdata_YYYY-MM.parquet '2019-06-15 09:00:00' '2019-06-15 18:00:00'

# Parquetファイルの情報（メタデータ）を表示する
$ ./target/release/parmetadata yellow_tripdata_YYYY-MM.parquet
```


## 気づいたことなど

- parquetクレートがコンパイルエラーになる
    - crates.ioに公開されている最新版（0.14.1）ではparquetクレート自体がコンパイルエラーなってしまう。とりあえずGitHub上の最新のコードを使用中
- yellow_tripdata CSVファイルのレコードはソートされていない
    - クエリの性能を高めるためにpickup_datetimeでソートする？
        - row groupの中だけでソートするのは簡単にできる
        - ファイルが巨大なので、全体をソートするにはワークファイルに書き出しながらバケットソートする必要がある
- Parquetは（JSONのような）入れ子になったデータ構造も表現できるので、そういうデータの扱いも試してみたい

use chrono::prelude::*;
use clap::{App, Arg};
use hello_parquet::schema::{get_f64_column_reader, get_i64_column_reader, ColIndex};
use parquet::file::reader::{FileReader, SerializedFileReader};
use smallbitvec::SmallBitVec;
use std::{
    error::Error,
    fs,
    path::{Path, PathBuf},
};

static BATCH_SIZE: usize = 10_050;
static DATE_FORMAT: &'static str = "%Y-%m-%d %H:%M:%S";

struct TripVec {
    selected_count: usize,
    bitvec: SmallBitVec,
    pickup_datetime: Vec<i64>,
    passenger_count: Vec<i32>,
    trip_distance: Vec<f64>,
}

impl TripVec {
    fn with_capacity(cap: usize) -> Self {
        Self {
            selected_count: 0,
            bitvec: SmallBitVec::with_capacity(cap),
            pickup_datetime: Vec::default(),
            passenger_count: Vec::default(),
            trip_distance: Vec::default(),
        }
    }

    fn reserve_pickup_datetime(&mut self) {
        let bitvec_cap = self.bitvec.capacity();
        let cap = self.pickup_datetime.capacity();
        if bitvec_cap > cap {
            self.pickup_datetime.reserve(bitvec_cap - cap);
        }
    }

    fn reserve_trip_distance(&mut self) {
        let bitvec_cap = self.bitvec.capacity();
        let cap = self.trip_distance.capacity();
        if bitvec_cap > cap {
            self.trip_distance.reserve(bitvec_cap - cap);
        }
    }

    fn clear(&mut self) {
        self.selected_count = 0;
        self.bitvec.clear();
        self.pickup_datetime.clear();
        self.passenger_count.clear();
        self.trip_distance.clear();
    }

    fn select_next_value(&mut self, selected: bool) {
        self.bitvec.push(selected);
        if selected {
            self.selected_count += 1;
        }
    }

    fn set_pickup_datetime(&mut self, data: &[i64]) {
        for (i, v) in data.iter().enumerate() {
            if self.bitvec[i] {
                self.pickup_datetime.push(*v);
            }
        }
    }

    fn set_trip_distance(&mut self, data: &[f64]) {
        for (i, v) in data.iter().enumerate() {
            if self.bitvec[i] {
                self.trip_distance.push(*v);
            }
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let (infile, search_start, search_end) = parse_args()?;
    let search_range = search_start..search_end;

    let print_limit = 10;

    let parquet = fs::File::open(infile)?;
    let reader = SerializedFileReader::new(parquet)?;
    let metadata = reader.metadata();

    let mut i64_values = vec![0i64; BATCH_SIZE];
    let mut f64_values = vec![0.0; BATCH_SIZE];

    let mut scanned_row_count = 0;
    let mut matching_row_count = 0;

    let mut records = TripVec::with_capacity(BATCH_SIZE);
    records.reserve_pickup_datetime();
    records.reserve_trip_distance();

    for row_group_index in 0..metadata.num_row_groups() {
        let mut row_group_reader = reader.get_row_group(row_group_index)?;

        let mut col_reader_pudt =
            get_i64_column_reader(&mut row_group_reader, ColIndex::PickupDateTime)?;
        let mut col_reader_td =
            get_f64_column_reader(&mut row_group_reader, ColIndex::TripDistance)?;

        loop {
            records.clear();

            let (num_values, _) =
                col_reader_pudt.read_batch(BATCH_SIZE, None, None, &mut i64_values)?;
            if num_values == 0 {
                break;
            };

            // find matching values
            for v in &i64_values[0..num_values] {
                records.select_next_value(search_range.contains(v));
            }
            records.set_pickup_datetime(&i64_values[0..num_values]);

            matching_row_count += records.selected_count;
            scanned_row_count += num_values;

            if records.selected_count > 0 || num_values == BATCH_SIZE {
                let (num_values, _) =
                    col_reader_td.read_batch(BATCH_SIZE, None, None, &mut f64_values)?;
                if records.selected_count > 0 {
                    records.set_trip_distance(&f64_values[0..num_values]);
                    print_records(&records, print_limit);
                }
            }
        }
    }

    println!(
        "Scanned {} rows. Found {} matching rows.",
        scanned_row_count, matching_row_count
    );

    Ok(())
}

fn parse_args() -> Result<(PathBuf, i64, i64), Box<dyn Error>> {
    let arg_matches = App::new("parselect1")
        .version("0.1")
        .about("Select matching records form a yellow cab trip data Parquet file")
        .arg(
            Arg::with_name("INFILE")
                .help("Sets the input Parquet file")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("FROM_DATETIME")
                .help("Sets the start date time to select (inclusive). e.g. '2019-06-15 15:00:00'")
                .required(true)
                .index(2),
        )
        .arg(
            Arg::with_name("TO_DATETIME")
                .help("Sets the end date time to select (exclusive)")
                .required(true)
                .index(3),
        )
        .get_matches();

    // Calling unwrap() is safe here because "INFILE" is required.
    let infile = Path::new(arg_matches.value_of("INFILE").unwrap()).to_path_buf();
    if !infile.is_file() {
        return Err(From::from("INFILE {} is not a regular file"));
    }
    let from_datetime = arg_matches.value_of("FROM_DATETIME").unwrap();
    let to_datetime = arg_matches.value_of("TO_DATETIME").unwrap();

    let search_start = Utc
        .datetime_from_str(from_datetime, DATE_FORMAT)?
        .timestamp_millis();
    let search_end = Utc
        .datetime_from_str(to_datetime, DATE_FORMAT)?
        .timestamp_millis();

    Ok((infile, search_start, search_end))
}

// TODO: ロック済みのstdoutを使う
fn print_records(records: &TripVec, print_limit: usize) {
    for i in 0..std::cmp::min(print_limit, records.selected_count) {
        let pickup_datetime = Utc.timestamp_millis(records.pickup_datetime[i]);
        println!(
            "{}|{}",
            pickup_datetime.format(DATE_FORMAT),
            records.trip_distance[i]
        );
    }
    if records.selected_count > print_limit {
        println!("(Omitted {} records)", records.selected_count - print_limit);
    }
    println!();
}

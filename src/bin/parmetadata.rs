use clap::{App, Arg};
use parquet::file::reader::{FileReader, SerializedFileReader};
use std::{
    error::Error,
    fs,
    path::{Path, PathBuf},
};

fn main() -> Result<(), Box<dyn Error>> {
    let path = parse_args()?;
    let parquet = fs::File::open(path)?;
    let reader = SerializedFileReader::new(parquet)?;
    let parquet_metadata = reader.metadata();
    let file_metadata = parquet_metadata.file_metadata();
    println!("column orders: {:?}", file_metadata.column_orders());

    if let Some(row_group_metadata) = parquet_metadata.row_groups().first() {
        let column_chunk_metadata_slice = row_group_metadata.columns();
        if column_chunk_metadata_slice.len() > 0 {
            for column_chunk_metadata in column_chunk_metadata_slice {
                let path = column_chunk_metadata.column_path();
                let primitive_type = column_chunk_metadata.column_type();
                let logical_type = column_chunk_metadata.column_descr_ptr().logical_type();
                let encodings = column_chunk_metadata.encodings();
                let compression = column_chunk_metadata.compression();
                let statistics = column_chunk_metadata.statistics();
                println!(
                    "{} {} ({}), encodings: {:?}, compression: {}, statistics: {:?}",
                    path.string(),
                    primitive_type,
                    logical_type,
                    encodings,
                    compression,
                    statistics,
                );
            }
        } else {
            println!("There is no column chunks.");
        }
    } else {
        println!("There is no row groups.");
    }

    Ok(())
}

fn parse_args() -> Result<PathBuf, Box<dyn Error>> {
    let arg_matches = App::new("parmetadata")
        .version("0.1")
        .about("Print some metadata of a yellow cab trip data Parquet file")
        .arg(
            Arg::with_name("INFILE")
                .help("Sets the input Parquet file")
                .required(true)
                .index(1),
        )
        .get_matches();

    // Calling unwrap() is safe here because "INFILE" is required.
    let infile = Path::new(arg_matches.value_of("INFILE").unwrap()).to_path_buf();
    if !infile.is_file() {
        return Err(From::from("INFILE {} is not a regular file"));
    }
    Ok(infile)
}

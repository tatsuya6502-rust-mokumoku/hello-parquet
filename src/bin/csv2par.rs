use clap::{App, Arg};
use hello_parquet::{schema, utils::deserialize_datetime_str};
use parquet::{
    column::writer::ColumnWriter,
    data_type::ByteArray,
    file::{
        properties::WriterProperties,
        writer::{FileWriter, RowGroupWriter, SerializedFileWriter},
    },
    // schema::types::ColumnPath,
};
use serde::Deserialize;
use std::{
    error::Error,
    fs,
    path::{Path, PathBuf},
    rc::Rc,
};

const BATCH_SIZE: usize = 10_000;

#[derive(Debug, Deserialize)]
struct Trip {
    #[serde(rename = "VendorID")]
    vendor_id: u8,
    #[serde(
        rename = "tpep_pickup_datetime",
        deserialize_with = "deserialize_datetime_str"
    )]
    pickup_datetime: i64,
    #[serde(
        rename = "tpep_dropoff_datetime",
        deserialize_with = "deserialize_datetime_str"
    )]
    dropoff_datetime: i64,
    passenger_count: i64,
    trip_distance: f64,
    #[serde(rename = "RatecodeID")]
    rate_code_id: u8, // TODO: Introduce enum.
    store_and_fwd_flag: String, // TODO: Introduce enum. (values are Y or N)
    #[serde(rename = "PULocationID")]
    pickup_location_id: u16,
    #[serde(rename = "DOLocationID")]
    dropoff_location_id: u16,
    payment_type: u8, // TODO: Introduce enum.
    // TODO: ENHANCEME: Use a decimal type for these currencies to avoid round off errors.
    fare_amount: f64,
    extra: f64,
    mta_tax: f64,
    tip_amount: f64,
    tolls_amount: f64,
    improvement_surcharge: f64,
    total_amount: f64,
    congestion_surcharge: Option<f64>,
}

struct TripVec {
    vendor_id: Vec<i32>,
    pickup_datetime: Vec<i64>,
    dropoff_datetime: Vec<i64>,
    passenger_count: Vec<i32>,
    trip_distance: Vec<f64>,
    rate_code_id: Vec<i32>,
    store_and_forward_flag: Vec<ByteArray>,
    pickup_location_id: Vec<i32>,
    dropoff_location_id: Vec<i32>,
    payment_type: Vec<i32>,
    fare_amount: Vec<f64>,
    extra: Vec<f64>,
    mta_tax: Vec<f64>,
    tip_amount: Vec<f64>,
    tolls_amount: Vec<f64>,
    improvement_surcharge: Vec<f64>,
    total_amount: Vec<f64>,
    congestion_surcharge: Vec<Option<f64>>,
}

impl TripVec {
    fn with_capacity(capacity: usize) -> Self {
        Self {
            vendor_id: Vec::with_capacity(capacity),
            pickup_datetime: Vec::with_capacity(capacity),
            dropoff_datetime: Vec::with_capacity(capacity),
            passenger_count: Vec::with_capacity(capacity),
            trip_distance: Vec::with_capacity(capacity),
            rate_code_id: Vec::with_capacity(capacity),
            store_and_forward_flag: Vec::with_capacity(capacity),
            pickup_location_id: Vec::with_capacity(capacity),
            dropoff_location_id: Vec::with_capacity(capacity),
            payment_type: Vec::with_capacity(capacity),
            fare_amount: Vec::with_capacity(capacity),
            extra: Vec::with_capacity(capacity),
            mta_tax: Vec::with_capacity(capacity),
            tip_amount: Vec::with_capacity(capacity),
            tolls_amount: Vec::with_capacity(capacity),
            improvement_surcharge: Vec::with_capacity(capacity),
            total_amount: Vec::with_capacity(capacity),
            congestion_surcharge: Vec::with_capacity(capacity),
        }
    }

    fn clear(&mut self) {
        self.vendor_id.clear();
        self.pickup_datetime.clear();
        self.dropoff_datetime.clear();
        self.passenger_count.clear();
        self.trip_distance.clear();
        self.rate_code_id.clear();
        self.store_and_forward_flag.clear();
        self.pickup_location_id.clear();
        self.dropoff_location_id.clear();
        self.payment_type.clear();
        self.fare_amount.clear();
        self.extra.clear();
        self.mta_tax.clear();
        self.tip_amount.clear();
        self.tolls_amount.clear();
        self.improvement_surcharge.clear();
        self.total_amount.clear();
        self.congestion_surcharge.clear();
    }

    fn push(&mut self, trip: Trip) -> Result<(), Box<dyn Error>> {
        self.vendor_id.push(trip.vendor_id as i32);
        self.pickup_datetime.push(trip.pickup_datetime);
        self.dropoff_datetime.push(trip.dropoff_datetime);
        self.passenger_count.push(trip.passenger_count as i32);
        self.trip_distance.push(trip.trip_distance);
        self.rate_code_id.push(trip.rate_code_id as i32);
        self.store_and_forward_flag
            .push(trip.store_and_fwd_flag.as_str().into());
        self.pickup_location_id.push(trip.pickup_location_id as i32);
        self.dropoff_location_id
            .push(trip.dropoff_location_id as i32);
        self.payment_type.push(trip.payment_type as i32);
        self.fare_amount.push(trip.fare_amount);
        self.extra.push(trip.extra);
        self.mta_tax.push(trip.mta_tax);
        self.tip_amount.push(trip.tip_amount);
        self.tolls_amount.push(trip.tolls_amount);
        self.improvement_surcharge.push(trip.improvement_surcharge);
        self.total_amount.push(trip.total_amount);
        self.congestion_surcharge.push(trip.congestion_surcharge);
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let (infile, outfile) = parse_args()?;

    let mut reader = csv::Reader::from_path(infile)?;

    let schema = schema::create_schema()?;

    // let prop_builder = WriterProperties::builder()
    //     .set_column_statistics_enabled(ColumnPath::new(vec!["pickup_datetime".to_string()]), true)
    //     .set_max_statistics_size(4090 * 1000);
    let prop_builder = WriterProperties::builder();
    let props = Rc::new(prop_builder.build());

    let parquet = fs::File::create(outfile)?;
    let mut writer = SerializedFileWriter::new(parquet, schema, props)?;

    let mut rows = TripVec::with_capacity(BATCH_SIZE);

    let mut count = 0;
    for result in reader.deserialize() {
        let record: Trip = result?;
        rows.push(record)?;
        count += 1;
        if count % BATCH_SIZE == 0 {
            write_rows(&mut writer, &mut rows)?;
            rows.clear();
        }
    }

    write_rows(&mut writer, &mut rows)?;
    writer.close()?;

    println!("Finished processing {} records.", count);
    Ok(())
}

fn parse_args() -> Result<(PathBuf, PathBuf), Box<dyn Error>> {
    let arg_matches = App::new("csv2par")
        .version("0.1")
        .about("Convert a yellow cab trip data CSV file to a Parquet file")
        .arg(
            Arg::with_name("INFILE")
                .help("Sets the input CSV file")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTFILE")
                .help("Sets the output Parquet file")
                .required(false)
                .index(2),
        )
        .get_matches();

    // Calling unwrap() is safe here because "INFILE" is required.
    let infile = Path::new(arg_matches.value_of("INFILE").unwrap()).to_path_buf();
    if !infile.is_file() {
        return Err(From::from("INFILE {} is not a regular file"));
    }

    let outfile: PathBuf = if let Some(s) = arg_matches.value_of("OUTFILE") {
        Path::new(s).to_path_buf()
    } else {
        let mut out = infile.clone();
        if out.set_extension("parquet") {
            out
        } else {
            return Err(From::from(format!(
                "Cannot change file extension of {}",
                infile.to_string_lossy()
            )));
        }
    };

    Ok((infile, outfile))
}

fn write_rows(writer: &mut SerializedFileWriter, rows: &TripVec) -> Result<(), Box<dyn Error>> {
    let mut row_group_writer = writer.next_row_group()?;

    write_int32_cols(&mut row_group_writer, &rows.vendor_id)?;
    write_int64_cols(&mut row_group_writer, &rows.pickup_datetime)?;
    write_int64_cols(&mut row_group_writer, &rows.dropoff_datetime)?;
    write_int32_cols(&mut row_group_writer, &rows.passenger_count)?;
    write_double_cols(&mut row_group_writer, &rows.trip_distance)?;
    write_int32_cols(&mut row_group_writer, &rows.rate_code_id)?;
    write_byte_array_cols(&mut row_group_writer, &rows.store_and_forward_flag)?;
    write_int32_cols(&mut row_group_writer, &rows.pickup_location_id)?;
    write_int32_cols(&mut row_group_writer, &rows.dropoff_location_id)?;
    write_int32_cols(&mut row_group_writer, &rows.payment_type)?;
    write_double_cols(&mut row_group_writer, &rows.fare_amount)?;
    write_double_cols(&mut row_group_writer, &rows.extra)?;
    write_double_cols(&mut row_group_writer, &rows.mta_tax)?;
    write_double_cols(&mut row_group_writer, &rows.tip_amount)?;
    write_double_cols(&mut row_group_writer, &rows.tolls_amount)?;
    write_double_cols(&mut row_group_writer, &rows.improvement_surcharge)?;
    write_double_cols(&mut row_group_writer, &rows.total_amount)?;
    // TODO: Write congestion_surcharge

    writer.close_row_group(row_group_writer)?;
    Ok(())
}

fn write_byte_array_cols(
    row_group_writer: &mut Box<dyn RowGroupWriter>,
    cols: &[ByteArray],
) -> Result<(), Box<dyn Error>> {
    if let Some(mut col_writer) = row_group_writer.next_column()? {
        if let ColumnWriter::ByteArrayColumnWriter(ref mut typed_writer) = col_writer {
            typed_writer.write_batch(cols, None, None)?;
        } else {
            return Err(From::from("Got a wrong column writer."));
        }
        row_group_writer.close_column(col_writer)?;
        Ok(())
    } else {
        Err(From::from("Got no column writer."))
    }
}

fn write_int32_cols(
    row_group_writer: &mut Box<dyn RowGroupWriter>,
    cols: &[i32],
) -> Result<(), Box<dyn Error>> {
    if let Some(mut col_writer) = row_group_writer.next_column()? {
        if let ColumnWriter::Int32ColumnWriter(ref mut typed_writer) = col_writer {
            typed_writer.write_batch(cols, None, None)?;
        } else {
            return Err(From::from("Got a wrong column writer."));
        }
        row_group_writer.close_column(col_writer)?;
        Ok(())
    } else {
        Err(From::from("Got no column writer."))
    }
}

fn write_int64_cols(
    row_group_writer: &mut Box<dyn RowGroupWriter>,
    cols: &[i64],
) -> Result<(), Box<dyn Error>> {
    if let Some(mut col_writer) = row_group_writer.next_column()? {
        if let ColumnWriter::Int64ColumnWriter(ref mut typed_writer) = col_writer {
            typed_writer.write_batch(cols, None, None)?;
        } else {
            return Err(From::from("Got a wrong column writer."));
        }
        row_group_writer.close_column(col_writer)?;
        Ok(())
    } else {
        Err(From::from("Got no column writer."))
    }
}

fn write_double_cols(
    row_group_writer: &mut Box<dyn RowGroupWriter>,
    cols: &[f64],
) -> Result<(), Box<dyn Error>> {
    if let Some(mut col_writer) = row_group_writer.next_column()? {
        if let ColumnWriter::DoubleColumnWriter(ref mut typed_writer) = col_writer {
            typed_writer.write_batch(cols, None, None)?;
        } else {
            return Err(From::from("Got a wrong column writer."));
        }
        row_group_writer.close_column(col_writer)?;
        Ok(())
    } else {
        Err(From::from("Got no column writer."))
    }
}

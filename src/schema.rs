use std::{error::Error, rc::Rc};

use parquet::schema::{parser::parse_message_type, types::Type as MessageType};
use parquet::{
    column::reader::{ColumnReader, ColumnReaderImpl},
    data_type::{DoubleType, Int64Type},
    file::reader::RowGroupReader,
};

static TRIP_SCHEMA: &'static str = "
    message schema {
        required INT32 vendor_id;
        required INT64 pickup_datetime (TIMESTAMP_MILLIS);
        required INT64 dropoff_datetime (TIMESTAMP_MILLIS);
        required INT32 passenger_count;
        required DOUBLE trip_distance;
        required INT32 rate_code_id;
        required BYTE_ARRAY store_and_forward_flag;
        required INT32 pickup_location_id;
        required INT32 dropoff_location_id;
        required INT32 payment_type;
        required DOUBLE fare_amount;
        required DOUBLE extra;
        required DOUBLE mta_tax;
        required DOUBLE improvement_surcharge;
        required DOUBLE tip_amount;
        required DOUBLE tolls_amount;
        required DOUBLE total_amount;
    }
    ";

//      optional DOUBLE congestion_surcharge;

pub enum ColIndex {
    VendorId = 0,
    PickupDateTime = 1,
    DropoffDateTime = 2,
    PassengerCount = 3,
    TripDistance = 4,
    RateCodeId = 5,
    StoreAndForwardFlag = 6,
    PickupLocationId = 7,
    DropoffLocationId = 8,
    PaymentType = 9,
    FareAmount = 10,
    Extra = 11,
    MTATax = 12,
    ImprovementSurcharge = 13,
    TipAmount = 14,
    TollsAmount = 15,
    TotalAmount = 16,
}

pub fn create_schema() -> Result<Rc<MessageType>, Box<dyn Error>> {
    Ok(Rc::new(dbg!(parse_message_type(TRIP_SCHEMA)?)))
}

pub fn get_i64_column_reader(
    row_group_reader: &mut Box<dyn RowGroupReader>,
    col_index: ColIndex,
) -> Result<ColumnReaderImpl<Int64Type>, Box<dyn Error>> {
    let column_reader = row_group_reader.get_column_reader(col_index as usize)?;
    if let ColumnReader::Int64ColumnReader(typed_reader) = column_reader {
        Ok(typed_reader)
    } else {
        Err(From::from("Got a wrong column reader"))
    }
}

pub fn get_f64_column_reader(
    row_group_reader: &mut Box<dyn RowGroupReader>,
    col_index: ColIndex,
) -> Result<ColumnReaderImpl<DoubleType>, Box<dyn Error>> {
    let column_reader = row_group_reader.get_column_reader(col_index as usize)?;
    if let ColumnReader::DoubleColumnReader(typed_reader) = column_reader {
        Ok(typed_reader)
    } else {
        Err(From::from("Got a wrong column reader"))
    }
}

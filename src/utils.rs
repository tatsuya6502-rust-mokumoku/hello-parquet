use chrono::{offset::TimeZone, Utc};
use serde::de;
use std::fmt;

static DATE_FORMAT: &'static str = "%Y-%m-%d %H:%M:%S";

pub fn deserialize_datetime_str<'de, D>(d: D) -> Result<i64, D::Error>
where
    D: de::Deserializer<'de>,
{
    Ok(d.deserialize_str(DateTimeVisitor)?)
}

struct DateTimeVisitor;

impl<'de> de::Visitor<'de> for DateTimeVisitor {
    type Value = i64;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a &str in {} format.", DATE_FORMAT))
    }

    fn visit_str<E>(self, value: &str) -> Result<i64, E>
    where
        E: de::Error,
    {
        Utc.datetime_from_str(value, DATE_FORMAT)
            .map(|d| d.timestamp_millis())
            .map_err(|e| E::custom(format!("{}", e)))
    }
}
